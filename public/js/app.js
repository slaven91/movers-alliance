(function () {
    'use strict';

    angular
        .module('app', ['ui.router', 'restangular', 'angular-jwt', 'angular-storage'])
        .config(config)
        .run(run);


    function config($stateProvider, $urlRouterProvider, $httpProvider, RestangularProvider, jwtInterceptorProvider) {
        $stateProvider
            .state('/', {
                url: "/",
                templateUrl: "js/home/home.html",
                controller: "HomeController",
                controllerAs: "HomeCtrl",
                data: {
                    requireLogin: true
                }
            })
            .state('register', {
                url: "/register",
                templateUrl: "js/auth/register.html",
                controller: "RegisterController",
                controllerAs: "RegisterCtrl"
            })
            .state('login', {
                url: "/login",
                templateUrl: "js/auth/login.html",
                controller: "LoginController",
                controllerAs: "LoginCtrl"
            });

        $urlRouterProvider.otherwise("/login");

        RestangularProvider.setBaseUrl("api");

        jwtInterceptorProvider.tokenGetter = ['store', function (store) {
            return store.get('token');
        }];

        $httpProvider.interceptors.push('jwtInterceptor');
    }

    function run($rootScope, $state, store) {
        $rootScope.$on('$stateChangeStart', function (e, to, toParams, fromState) {
            if (to.data && to.data.requireLogin) {
                if (!store.get('token')) {
                    e.preventDefault();
                    $state.go('login');
                }
            }
        });
    }


})();