(function () {
    'use strict';

    angular
        .module('app')
        .controller('HomeController', HomeController);

    function HomeController(store, $state) {
        var vm = this;

        vm.logout = logout;

        function logout() {
            store.remove('token');
            $state.reload();
        }
    }

})();
