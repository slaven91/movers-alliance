(function() {
    'use strict';

    angular
        .module('app')
        .factory('auth', auth);

    function auth(Restangular) {

        return {
            login: login,
            register: register
        };

        function login(data) {
            return Restangular.all('login').post({
                email: data.email,
                password: data.password
            });
        }

        function register(data) {
            return Restangular.all('register').post({
                email: data.email,
                password: data.password
            });
        }

    }

})();
