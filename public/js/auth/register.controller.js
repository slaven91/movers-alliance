(function () {
    'use strict';

    angular
        .module('app')
        .controller('RegisterController', RegisterController);

    function RegisterController(auth, store) {
        var vm = this;

        vm.data = {
            email: '',
            password: ''
        };

        vm.register = register;

        function register() {
            auth.register(vm.data).then(success());
        }

        function success(data) {
            store.set('token', data);
        }
    }

})();
