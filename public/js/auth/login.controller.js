(function () {
    'use strict';

    angular
        .module('app')
        .controller('LoginController', LoginController);

    function LoginController(auth, store, $state) {
        var vm = this;

        vm.data = {
            email: '',
            password: ''
        };

        vm.login = login;

        function login() {
            auth.login(vm.data).then(function (data) {
                store.set('token', data.token);
                $state.go('/');
            });
        }

    }

})();
