<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;

class RegisterController extends Controller
{
    protected function create(Request $request)
    {
        $email = $request->email;
        $password = $request->password;

        return User::create([
            'name' => 'test user',
            'email' => $email,
            'password' => bcrypt($password)
        ]);
    }
}
