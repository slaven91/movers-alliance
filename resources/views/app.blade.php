<!DOCTYPE html>
<html lang="en" ng-app="app">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Movers Alliance</title>

    <link rel="stylesheet" href="bower_components/bootstrap/dist/css/bootstrap.css">
    <link rel="stylesheet" href="bower_components/font-awesome/css/font-awesome.css">
    <link rel="stylesheet" href="css/style.css">

    <script src="bower_components/angular/angular.js"></script>
    <script src="bower_components/jquery/dist/jquery.js"></script>
    <script src="bower_components/bootstrap/dist/js/bootstrap.js"></script>
    <script src="bower_components/angular-ui-router/release/angular-ui-router.js"></script>
    <script src="bower_components/lodash/dist/lodash.js"></script>
    <script src="bower_components/restangular/dist/restangular.js"></script>
    <script src="bower_components/angular-jwt/dist/angular-jwt.js"></script>
    <script src="bower_components/a0-angular-storage/dist/angular-storage.js"></script>
</head>
<body>
<section ui-view></section>

<!-- Scripts -->
    <script src="js/app.js"></script>
    <script src="js/auth/auth.factory.js"></script>
    <script src="js/auth/login.controller.js"></script>
    <script src="js/auth/register.controller.js"></script>
    <script src="js/home/home.controller.js"></script>
</body>
</html>
